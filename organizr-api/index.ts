/**
 * Organizr - REST API
 * 0.0.1
 * DO NOT MODIFY - This file has been generated using oazapfts.
 * See https://www.npmjs.com/package/oazapfts
 */
import * as Oazapfts from "oazapfts/lib/runtime";
import * as QS from "oazapfts/lib/runtime/query";
export const defaults: Oazapfts.RequestOpts = {
    baseUrl: "http://localhost:7990/bitbucket/rest/organizr/latest",
};
const oazapfts = Oazapfts.runtime(defaults);
export const servers = {
    developmentUrl: "http://localhost:7990/bitbucket/rest/organizr/latest"
};
export type RestPullRequestParticipant = {
    role?: "AUTHOR" | "REVIEWER" | "PARTICIPANT";
    status?: "UNAPPROVED" | "NEEDS_WORK" | "APPROVED";
    user: {
        links?: {
            empty?: boolean;
        };
        slug: string;
        active: boolean;
        id: number;
        "type"?: "NORMAL" | "SERVICE";
        displayName: string;
        emailAddress: string;
        name: string;
        avatarUrl?: string;
        empty?: boolean;
    };
    empty?: boolean;
};
export type RestPullRequest = {
    title?: string;
    toRef?: {
        repository?: {
            slug?: string;
            project?: {
                description?: string;
                "namespace"?: string;
                avatar?: string;
                avatarUrl?: string;
                name?: string;
                key?: string;
                id?: number;
                "type"?: "NORMAL" | "PERSONAL";
                "public"?: boolean;
                links?: {
                    empty?: boolean;
                };
                empty?: boolean;
            };
            description?: string;
            defaultBranch?: string;
            relatedLinks?: {
                empty?: boolean;
            };
            scmId?: string;
            forkable?: boolean;
            name?: string;
            id?: number;
            "public"?: boolean;
            links?: {
                empty?: boolean;
            };
            empty?: boolean;
        };
        id?: string;
        empty?: boolean;
        displayId?: string;
    };
    fromRef?: {
        repository?: {
            slug?: string;
            project?: {
                description?: string;
                "namespace"?: string;
                avatar?: string;
                avatarUrl?: string;
                name?: string;
                key?: string;
                id?: number;
                "type"?: "NORMAL" | "PERSONAL";
                "public"?: boolean;
                links?: {
                    empty?: boolean;
                };
                empty?: boolean;
            };
            description?: string;
            defaultBranch?: string;
            relatedLinks?: {
                empty?: boolean;
            };
            scmId?: string;
            forkable?: boolean;
            name?: string;
            id?: number;
            "public"?: boolean;
            links?: {
                empty?: boolean;
            };
            empty?: boolean;
        };
        id?: string;
        empty?: boolean;
        displayId?: string;
    };
    version?: number;
    description?: string;
    reviewers: RestPullRequestParticipant[];
    htmlDescription?: string;
    id: number;
    links?: {
        empty?: boolean;
        self: {
            href?: string;
        }[];
    };
    empty?: boolean;
    state: "DECLINED" | "MERGED" | "OPEN";
};
export type RestPullRequestRef = {
    repositoryId: number;
    pullRequestId: number;
};
export type RestDependency = {
    repositoryId: number;
    pullRequestId: number;
    requiredBy?: RestPullRequestRef[];
    requires?: RestPullRequestRef[];
};
export type GlobalConfigModel = {
    indexPeriod?: number;
    reindex?: boolean;
    indexEnabled?: boolean;
    indexVersion?: number;
    maxDependencyDepth?: number;
};
export type RestColumn = {
    name: "pullrequest" | "title" | "labels" | "duedate" | "description" | "reviewers" | "tasks" | "buildresult";
    title: string;
    enabled: boolean;
    width: "narrow" | "medium" | "wide";
    defaultColumn: boolean;
};
export type RestColumnConfig = {
    columns: RestColumn[];
    projKey?: string;
    repoSlug?: string;
};
export type RestLabels = {
    labels?: string[];
};
export type RestMetadata = {
    dueDate?: string;
};
export type RestQueryFilter = {
    id?: number;
    query?: string;
    queryName?: string;
    queryScope?: string;
    createdDate?: number;
    version?: number;
    author?: {
        links?: {
            empty?: boolean;
        };
        slug?: string;
        active?: boolean;
        id?: number;
        "type"?: "NORMAL" | "SERVICE";
        displayName?: string;
        emailAddress?: string;
        name?: string;
        avatarUrl?: string;
        empty?: boolean;
    };
};
export type RestBuildStatus = {
    state: "SUCCESSFUL" | "FAILED" | "INPROGRESS";
    key: string;
    name: string;
    url: string;
    description: string;
    dateAdded: number;
};
export type RestMetaPullRequest = {
    pullRequest: RestPullRequest;
    labels?: string[];
    dueDate?: string;
    buildStatuses: RestBuildStatus[];
    openTaskCount: number;
    resolvedTaskCount: number;
    requires: RestPullRequestRef[];
    requiredBy: RestPullRequestRef[];
    matchesFilter?: boolean;
};
/**
 * Resolves all pull requests that need to be merged before this pull request can be merged, also referred to as 'children'.
 */
export function getRequired(repositoryId: number, pullRequestId: number, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: RestPullRequest[];
    }>(`/dependency/${repositoryId}/${pullRequestId}/required`, {
        ...opts
    }));
}
/**
 * Resolves the pull request that relies on this pull request to be merged before the relying pull request can be merged, also referred to as 'parent'.
 */
export function getRequiredBy(repositoryId: number, pullRequestId: number, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: RestPullRequest[];
    }>(`/dependency/${repositoryId}/${pullRequestId}/requiredBy`, {
        ...opts
    }));
}
export function link(repositoryId: number, pullRequestId: number, restPullRequestRef: RestPullRequestRef, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: 200;
        data: RestDependency[];
    } | {
        status: 400;
        data: string;
    }>(`/dependency/${repositoryId}/${pullRequestId}`, oazapfts.json({
        ...opts,
        method: "POST",
        body: restPullRequestRef
    })));
}
export function unlink(repositoryId: number, pullRequestId: number, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: string;
    }>(`/dependency/${repositoryId}/${pullRequestId}`, {
        ...opts,
        method: "DELETE"
    }));
}
export function getGlobalConfig(opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: string;
    }>("/globalcfg", {
        ...opts
    }));
}
export function setGlobalConfig(globalConfigModel: GlobalConfigModel, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: string;
    }>("/globalcfg", oazapfts.json({
        ...opts,
        method: "POST",
        body: globalConfigModel
    })));
}
export function getReindexStatus(opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: string;
    }>("/globalcfg/reindexstatus", {
        ...opts
    }));
}
export function getGlobalColumnConfig(opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: string;
    }>("/globalcfg/columnConfig", {
        ...opts
    }));
}
export function setGlobalColumnConfig(restColumnConfig: RestColumnConfig, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: string;
    }>("/globalcfg/columnConfig", oazapfts.json({
        ...opts,
        method: "POST",
        body: restColumnConfig
    })));
}
export function getProjectColumnConfig(projKey: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: string;
    }>(`/globalcfg/columnConfig/project/${projKey}`, {
        ...opts
    }));
}
export function setProjectColumnConfig(projKey: string, restColumnConfig: RestColumnConfig, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: string;
    }>(`/globalcfg/columnConfig/project/${projKey}`, oazapfts.json({
        ...opts,
        method: "POST",
        body: restColumnConfig
    })));
}
export function getRepoColumnConfig(projKey: string, repoSlug: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: string;
    }>(`/globalcfg/columnConfig/project/${projKey}/repo/${repoSlug}`, {
        ...opts
    }));
}
export function setRepoColumnConfig(projKey: string, repoSlug: string, restColumnConfig: RestColumnConfig, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: string;
    }>(`/globalcfg/columnConfig/project/${projKey}/repo/${repoSlug}`, oazapfts.json({
        ...opts,
        method: "POST",
        body: restColumnConfig
    })));
}
export function findLabels({ q, page, size }: {
    q?: string;
    page?: number;
    size?: number;
} = {}, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: string;
    }>(`/labels${QS.query(QS.form({
        q,
        page,
        size
    }))}`, {
        ...opts
    }));
}
export function getLabels(projKey: string, repoSlug: string, id: number, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: string;
    }>(`/labels/${projKey}/${repoSlug}/${id}`, {
        ...opts
    }));
}
export function setLabels(projKey: string, repoSlug: string, id: number, restLabels: RestLabels, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: string;
    }>(`/labels/${projKey}/${repoSlug}/${id}`, oazapfts.json({
        ...opts,
        method: "POST",
        body: restLabels
    })));
}
export function deleteLabels(projKey: string, repoSlug: string, id: number, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: string;
    }>(`/labels/${projKey}/${repoSlug}/${id}`, {
        ...opts,
        method: "DELETE"
    }));
}
export function findPullRequests({ q, page, size }: {
    q?: string;
    page?: number;
    size?: number;
} = {}, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: string;
    }>(`/labels/pull-requests${QS.query(QS.form({
        q,
        page,
        size
    }))}`, {
        ...opts
    }));
}
export function getDueDate(projKey: string, repoSlug: string, id: number, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: string;
    }>(`/meta/duedate/${projKey}/${repoSlug}/${id}`, {
        ...opts
    }));
}
export function setDueDate(projKey: string, repoSlug: string, id: number, restMetadata: RestMetadata, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: string;
    }>(`/meta/duedate/${projKey}/${repoSlug}/${id}`, oazapfts.json({
        ...opts,
        method: "POST",
        body: restMetadata
    })));
}
export function deleteDueDate(projKey: string, repoSlug: string, id: number, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: string;
    }>(`/meta/duedate/${projKey}/${repoSlug}/${id}`, {
        ...opts,
        method: "DELETE"
    }));
}
export function getFilters(opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: string;
    }>("/filters", {
        ...opts
    }));
}
export function addQueryFilter(restQueryFilter: RestQueryFilter, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: string;
    }>("/filters", oazapfts.json({
        ...opts,
        method: "POST",
        body: restQueryFilter
    })));
}
export function updateQueryFilter(id: number, restQueryFilter: RestQueryFilter, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: string;
    }>(`/filters/${id}`, oazapfts.json({
        ...opts,
        method: "PUT",
        body: restQueryFilter
    })));
}
export function deleteQuery(id: number, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: string;
    }>(`/filters/${id}`, {
        ...opts,
        method: "DELETE"
    }));
}
export function getPullRequests({ q, page, size }: {
    q?: string;
    page?: number;
    size?: number;
} = {}, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: RestMetaPullRequest[];
    }>(`/pull-requests${QS.query(QS.form({
        q,
        page,
        size
    }))}`, {
        ...opts
    }));
}
export function getPullRequest(projKey: string, repoSlug: string, id: number, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: string;
    }>(`/pull-requests/${projKey}/${repoSlug}/${id}`, {
        ...opts
    }));
}
export function getPullRequestIndexDoc(projKey: string, repoSlug: string, id: number, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: string;
    }>(`/pull-requests/index/${projKey}/${repoSlug}/${id}`, {
        ...opts
    }));
}
export function indexPullRequest(projKey: string, repoSlug: string, id: number, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: string;
    }>(`/pull-requests/index/${projKey}/${repoSlug}/${id}`, {
        ...opts,
        method: "POST"
    }));
}
